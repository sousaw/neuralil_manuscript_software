# Copyright 2019-2021 The NeuralIL contributors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Callable
from typing import Sequence

import jax
import jax.nn
import jax.numpy as jnp
import flax.linen
from neuralil.neural_network.bessel_descriptors import center_at_atoms


class BasicMLP(flax.linen.Module):
    layer_widths: Sequence[int]
    activation_function: Callable = flax.linen.swish

    @flax.linen.compact
    def __call__(self, descriptors):
        result = self.activation_function(
            flax.linen.Dense(self.layer_widths[0])(descriptors))
        for w in self.layer_widths[1:]:
            result = self.activation_function(flax.linen.LayerNorm()(
                flax.linen.Dense(w, use_bias=False)(result)))
        return result


class Core(flax.linen.Module):
    """Multilayer perceptron with LayerNorm lying at the core of the model.

    This model takes the descriptors of each atom (Bessel + embedding,
    concatenated or otherwise combined) as inputs and calculates that atom's
    contribution to the potential energy. LayerNorm is applied at each layer
    except the first and the last ones.

    Args:
        layer_widths: The sequence of layer widths, excluding the output
            layer, which always has a width equal to one.
        activation_function: The nonlinear activation function for each neuron,
            which is Swish by default.
    """
    layer_widths: Sequence[int]
    activation_function: Callable = flax.linen.swish

    @flax.linen.compact
    def __call__(self, descriptors):
        result = self.activation_function(
            flax.linen.Dense(self.layer_widths[0])(descriptors))
        for w in self.layer_widths[1:]:
            result = self.activation_function(flax.linen.LayerNorm()(
                flax.linen.Dense(w, use_bias=False)(result)))
        return self.activation_function(flax.linen.Dense(1)(result))


class CoreWithLinear(flax.linen.Module):
    layer_widths: Sequence[int]
    activation_function: Callable = flax.linen.swish

    @flax.linen.compact
    def __call__(self, descriptors):
        result = self.activation_function(
            flax.linen.Dense(self.layer_widths[0])(descriptors))
        for w in self.layer_widths[1:]:
            result = self.activation_function(flax.linen.LayerNorm()(
                flax.linen.Dense(w, use_bias=False)(result)))
        return flax.linen.Dense(1)(result)


class NeuralILCore(flax.linen.Module):
    """Legacy core architecture used in NeuralIL.

    Just like Core, this model takes the descriptors of each atom (Bessel +
    embedding, concatenated or otherwise combined) as inputs and calculates
    that atom's contribution to the potential energy. However, it does not apply
    any normalization but instead uses the SELU self-normalizing activation
    function. For historical reasons, it also appends an extra Swish layer
    with a width of one.

    Args:
        layer_widths: The sequence of layer widths, excluding the two output
            layer, which always have a width equal to one.
    """
    layer_widths: Sequence[int]

    @flax.linen.compact
    def __call__(self, descriptors):
        result = descriptors
        for w in self.layer_widths:
            result = jax.nn.selu(flax.linen.Dense(w)(result))
        result = jax.nn.selu(flax.linen.Dense(1)(result))
        result = flax.linen.swish(flax.linen.Dense(1)(result))
        return result


class DynamicsModel(flax.linen.Module):
    """Wrapper model around the core layers to calculate energies and forces.

    The class does not provide a __call__ method, forcing the user to choose
    what to evaluate (forces, energies or both).

    Args:
        n_types: The number of atomic types in the system.
        embed_d: The dimension of the embedding vector to be mixed with the
            descriptors.
        descriptor_generator: The function mapping the atomic coordinates,
            types and cell size to descriptors.
        core_model: The model that takes all the descriptors and returns an
            atomic contribution to the energy.
        mixer: The function that takes the Bessel descriptors and the
            embedding vectors and creates the input descriptors for the core
            model. The default choice just concatenates them.
    """
    n_types: int
    embed_d: int
    descriptor_generator: Callable
    core_model: flax.linen.Module
    mixer: Callable = lambda d, e: jnp.concatenate(
        [d.reshape((d.shape[0], -1)), e], axis=1)

    def setup(self):
        # These neurons create the embedding vector.
        self.embed = flax.linen.Embed(self.n_types, self.embed_d)
        # This linear layer centers and scales the energy after the core
        # has done its job.
        self.denormalizer = flax.linen.Dense(1)
        # The checkpointing strategy can be reconsidered to achieve different
        # tradeoffs between memory and CPU usage.
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_potential_energy_from_descriptors(self, combined_inputs):
        contributions = self.core_model(combined_inputs)
        contributions = self.denormalizer(contributions)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_atomic_energies(self, positions, types, cell):
        """Compute the atomic contributions to the potential energy.

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The n_atoms contributions to the energy.
        """
        descriptors = self.descriptor_generator(positions, types, cell)
        embeddings = self.embed(types)
        combined_inputs = self.mixer(descriptors, embeddings)
        results = self.core_model(combined_inputs)
        results = self.denormalizer(results)
        return results

    def calc_potential_energy(self, positions, types, cell):
        """Compute the total potential energy of the system.

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The sum of all atomic contributions to the potential energy.
        """
        contributions = self.calc_atomic_energies(positions, types, cell)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_forces(self, positions, types, cell):
        """Compute the force on each atom.

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The (n_atoms, 3) vector containing all the forces.
        """
        return -self._gradient_calculator(positions, types, cell)


class NoEmbeddingDynamicsModel(flax.linen.Module):
    n_types: int
    descriptor_generator: Callable
    core_model: flax.linen.Module

    def setup(self):
        self.denormalizer = flax.linen.Dense(1)
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_atomic_energies(self, positions, types, cell):
        descriptors = self.descriptor_generator(positions, types, cell)
        results = self.core_model(
            descriptors.reshape((descriptors.shape[0], -1)))
        results = self.denormalizer(results)
        return results

    def calc_potential_energy(self, positions, types, cell):
        contributions = self.calc_atomic_energies(positions, types, cell)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_forces(self, positions, types, cell):
        return -self._gradient_calculator(positions, types, cell)


class WeightEmbeddingDynamicsModel(flax.linen.Module):
    n_types: int
    descriptor_generator: Callable
    core_model: flax.linen.Module

    def setup(self):
        self.embed_d = (self.n_types * (self.n_types + 1)) // 2
        self.embed = flax.linen.Embed(self.n_types, self.embed_d)
        self.denormalizer = flax.linen.Dense(1)
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_atomic_energies(self, positions, types, cell):
        descriptors = self.descriptor_generator(positions, types, cell)
        embeddings = self.embed(types)
        internal_inputs = (descriptors *
                           embeddings[:, :, jnp.newaxis]).sum(axis=1)
        results = self.core_model(internal_inputs)
        return results

    def calc_potential_energy(self, positions, types, cell):
        contributions = self.calc_atomic_energies(positions, types, cell)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_forces(self, positions, types, cell):
        return -self._gradient_calculator(positions, types, cell)


class ZEmbeddingDynamicsModel(flax.linen.Module):
    n_types: int
    descriptor_generator: Callable
    core_model: flax.linen.Module

    def setup(self):
        self.denormalizer = flax.linen.Dense(1)
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_atomic_energies(self, positions, types, cell):
        descriptors = self.descriptor_generator(positions, types, cell)
        # The elements are listed in alphabetical order:
        # ['C', 'H', 'N', 'O']
        # atomic_numbers = jnp.array([6, 1, 7, 8], dtype=jnp.float32)
        # weights = jnp.outer(atomic_numbers, atomic_numbers)
        # triu_indices = jnp.triu_indices_from(weights)
        # weights = weights[triu_indices]
        internal_inputs = (
            36. * descriptors[:, 0, :] + 6. * descriptors[:, 1, :] +
            42. * descriptors[:, 2, :] + 48. * descriptors[:, 3, :] +
            1. * descriptors[:, 4, :] + 7. * descriptors[:, 5, :] +
            8. * descriptors[:, 6, :] + 49. * descriptors[:, 7, :] +
            56. * descriptors[:, 8, :] + 64. * descriptors[:, 9, :]) / 317.
        results = self.core_model(internal_inputs)
        return results

    def calc_potential_energy(self, positions, types, cell):
        contributions = self.calc_atomic_energies(positions, types, cell)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_forces(self, positions, types, cell):
        return -self._gradient_calculator(positions, types, cell)


class ChiEmbeddingDynamicsModel(flax.linen.Module):
    n_types: int
    descriptor_generator: Callable
    core_model: flax.linen.Module

    def setup(self):
        self.denormalizer = flax.linen.Dense(1)
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_atomic_energies(self, positions, types, cell):
        descriptors = self.descriptor_generator(positions, types, cell)
        # The elements are listed in alphabetical order:
        # ['C', 'H', 'N', 'O']
        chi = jnp.array([2.55, 2.20, 3.04, 3.44], dtype=jnp.float32)
        weights = jnp.outer(chi, chi)
        triu_indices = jnp.triu_indices_from(weights)
        weights = weights[triu_indices]
        embeddings = jnp.tile(weights, (len(types), 1))
        internal_inputs = (descriptors * embeddings[:, :, jnp.newaxis]).sum(
            axis=1) / weights.sum()
        results = self.core_model(internal_inputs)
        return results

    def calc_potential_energy(self, positions, types, cell):
        contributions = self.calc_atomic_energies(positions, types, cell)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_forces(self, positions, types, cell):
        return -self._gradient_calculator(positions, types, cell)


class TwoStageModel(flax.linen.Module):
    n_types: int
    embed_d: int
    descriptor_generator: Callable
    core_model: flax.linen.Module
    denormalizer_width: int
    mu_model: flax.linen.Module
    mixer: Callable = lambda d, e: jnp.concatenate(
        [d.reshape((d.shape[0], -1)), e], axis=1)

    def setup(self):
        self.embed = flax.linen.Embed(self.n_types, self.embed_d)
        self.denormalizer = flax.linen.Dense(self.denormalizer_width)
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_omega(self, positions, types, cell):
        descriptors = self.descriptor_generator(positions, types, cell)
        embeddings = self.embed(types)
        combined_inputs = self.mixer(descriptors, embeddings)
        results = self.denormalizer(self.core_model(combined_inputs))
        return results

    def calc_potential_energy(self, positions, types, cell):
        contributions = self.calc_omega(positions, types, cell)
        omega = contributions.mean(axis=0)
        return jnp.squeeze(self.mu_model(omega))

    def calc_forces(self, positions, types, cell):
        return -self._gradient_calculator(positions, types, cell)


class ElectrostaticModel(flax.linen.Module):
    """Implementation of the charge equilibration scheme in [1], 
    based on atomic electronegativities predicted by a neural network
    with a similar architecture as DynamicsModel. The widths of the
    charge densities (sigmas) should be provided and are typically
    taken to be the covalent radii. The atomic hardness is a 
    learnable parameter.
    
    References:
    [1] Rappe, A. K. & Goddard, W. A. J. Phys. Chem. 95, 3358 (1991).

    Args:
        core_model: The model that takes all the descriptors and returns an
            atomic electronegativities
        n_types: Number of atomic types in the system
        sigmas: Sequence of widths of the element-specific Gaussian 
            charge densities 
    """
    core_model: Sequence[int]
    n_types: int
    sigmas: Sequence[float]

    def setup(self):
        self.denormalizer = flax.linen.Dense(1)
        self.hardness = self.param('hardness', flax.linen.initializers.ones,
                                   (self.n_types, ))

    def calc_electronegativities_from_descriptors(self, combined_inputs):
        """Compute the electronegativities from mixed descriptors

        Args:
            combined_inputs: mixed descriptors

        Returns:
            The n_atoms electronegativities
        """
        results = self.core_model(combined_inputs)
        results = self.denormalizer(results)
        return jnp.squeeze(results)

    def _get_charges(self, combined_inputs, positions, types, cell):
        electronegativities = self.calc_electronegativities_from_descriptors(
            combined_inputs)
        sigmas = self.sigmas
        sigmas_expanded = jnp.take(jnp.array(sigmas), types)

        hardness_expanded = jnp.take(self.hardness, types)

        sq_sigmas = jnp.squeeze(sigmas_expanded * sigmas_expanded)
        gammas = jnp.sqrt(sq_sigmas[:, jnp.newaxis] + sq_sigmas)

        _, r_ij = center_at_atoms(positions, cell)

        diag = (hardness_expanded + 1. / (sigmas_expanded * jnp.sqrt(jnp.pi)))

        offdiag_ij = (jax.lax.erf(r_ij / (jnp.sqrt(2) * gammas)) /
                      (jnp.where(r_ij == 0, 1, r_ij)))

        a_ij = jnp.diag(diag) + offdiag_ij

        a_ij = jnp.vstack((a_ij, jnp.ones(a_ij.shape[0])))
        a_ij = jnp.hstack((a_ij, [[1]] * a_ij.shape[1] + [[0]]))
        electronegativities_sol = jnp.append(electronegativities, 0)

        charges = jax.scipy.linalg.solve(a_ij,
                                         (-1 * electronegativities_sol))[:-1]
        return offdiag_ij, charges

    def get_charges(self, combined_inputs, positions, types, cell):
        # Passthrough function to access charges
        return self._get_charges(combined_inputs, positions, types, cell)[1]

    @flax.linen.compact
    def __call__(self, combined_inputs, positions, types, cell):
        """Compute electrostatic contributions to potential energy

        Args:
            combined_inputs : Mixed descriptors
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The n_atoms electrostatic contributions to the energy
        """
        sigmas = self.sigmas
        sigmas_expanded = jnp.take(jnp.array(sigmas), types)
        offdiag, charges = self._get_charges(combined_inputs, positions, types,
                                             cell)

        self_contribution = ((charges * charges) /
                             (2 * sigmas_expanded * jnp.sqrt(jnp.pi))).sum()
        pair_contribution = jnp.dot(jnp.dot(offdiag, charges), charges) / 2.
        return jnp.squeeze(self_contribution + pair_contribution)


class DynamicsModelWithCharges(flax.linen.Module):
    """Wrapper model around the core layers and the electrostatic model
     to calculate energies and forces.

    The class does not provide a __call__ method, forcing the user to choose
    what to evaluate (forces, energies or both).

    Args:
        n_types: The number of atomic types in the system.
        embed_d: The dimension of the embedding vector to be mixed with the
            descriptors.
        descriptor_generator: The function mapping the atomic coordinates,
            types and cell size to descriptors.
        core_model: The model that takes all the descriptors and returns an
            atomic contribution to the energy.
        electrostatic_model: The model that takes all the descriptors and returns
            atomic electronegativities
        mixer: The function that takes the Bessel descriptors and the
            embedding vectors and creates the input descriptors for the core
            model. The default choice just concatenates them.
    """
    n_types: int
    embed_d: int
    descriptor_generator: Callable
    core_model: flax.linen.Module
    electrostatic_model: flax.linen.Module
    mixer: Callable = lambda d, e: jnp.concatenate(
        [d.reshape((d.shape[0], -1)), e], axis=1)

    def setup(self):
        # These neurons create the embedding vector.
        self.embed = flax.linen.Embed(self.n_types, self.embed_d)
        # This linear layer centers and scales the energy after the core
        # has done its job.
        self.denormalizer = flax.linen.Dense(1)
        # The checkpointing strategy can be reconsidered to achieve different
        # tradeoffs between memory and CPU usage.
        self._gradient_calculator = jax.checkpoint(
            jax.grad(self.calc_potential_energy, argnums=0))

    def calc_atomic_energies(self, positions, types, cell):
        """Compute the atomic contributions to the potential energy.

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The n_atoms contributions to the energy.
        """
        descriptors = self.descriptor_generator(positions, types, cell)
        embeddings = self.embed(types)
        combined_inputs = self.mixer(descriptors, embeddings)
        results = self.core_model(combined_inputs)
        results = self.denormalizer(results)
        electrostatic_energy = self.electrostatic_model(
            combined_inputs, positions, types, cell)
        return results + electrostatic_energy

    def calc_potential_energy(self, positions, types, cell):
        """Compute the total potential energy of the system.

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The sum of all atomic contributions to the potential energy.
        """
        contributions = self.calc_atomic_energies(positions, types, cell)
        return jnp.squeeze(contributions.sum(axis=0))

    def calc_forces(self, positions, types, cell):
        """Compute the force on each atom.

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The (n_atoms, 3) vector containing all the forces.
        """
        return -self._gradient_calculator(positions, types, cell)

    def calc_charges(self, positions, types, cell):
        """Computes atomic charges 
        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The n_atoms vector containing all the charges.
        """
        descriptors = self.descriptor_generator(positions, types, cell)
        embeddings = self.embed(types)
        combined_inputs = self.mixer(descriptors, embeddings)
        charges = self.electrostatic_model.get_charges(combined_inputs,
                                                       positions, types, cell)
        return charges

    def calc_electronegativities(self, positions, types, cell):
        """Computes atomic electronegativities

        Args:
            positions: The (n_atoms, 3) vector with the Cartesian coordinates
                of each atom.
            types: The atomic types, codified as integers from 0 to n_atoms - 1.
            cell: The (3, 3) matrix representing the simulation box ir periodic
                boundary conditions are in effect, or None otherwise.

        Returns:
            The n_atoms vector containing all the electronegativities.
        """
        descriptors = self.descriptor_generator(positions, types, cell)
        embeddings = self.embed(types)
        combined_inputs = self.mixer(descriptors, embeddings)
        electronegativities = (
            self.electrostatic_model.calc_electronegativities_from_descriptors(
                combined_inputs))
        return electronegativities
