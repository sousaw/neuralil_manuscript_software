#!/usr/bin/env python

# %%
import pathlib
import json
import pickle
import random
import collections

import numpy as onp
import tqdm.auto

import jax

jax.config.update("jax_platform_name", "cpu")
import jax.numpy as jnp
import flax
import flax.optim
import flax.jax_utils
import flax.serialization

from neuralil.neural_network.bessel_descriptors import (
    get_max_number_of_neighbors
)
from neuralil.neural_network.bessel_descriptors import PowerSpectrumGenerator
from neuralil.neural_network.model import Core
from neuralil.neural_network.model import DynamicsModel
from neuralil.neural_network.model import NoEmbeddingDynamicsModel
from neuralil.neural_network.model import ChiEmbeddingDynamicsModel
from neuralil.neural_network.model import ZEmbeddingDynamicsModel
from neuralil.neural_network.model import WeightEmbeddingDynamicsModel
from neuralil.neural_network.model import DynamicsModelWithCharges
from neuralil.neural_network.model import ElectrostaticModel

import matplotlib
import matplotlib.pyplot as plt

import scipy
import scipy.stats

# %%
random.seed(31337)

N_PAIR = 15

TRAINING_FRACTION = .9
R_CUT = 3.5
N_MAX = 4
EMBED_D = 2

charge_cation = [
    -0.627, 0.397, 0.392, 0.392, -0.131, 0.214, 0.214, -0.377, 0.161, 0.161,
    0.204
]
charge_anion = [0.794, -0.598, -0.598, -0.598]
charges = jnp.array(N_PAIR * charge_cation + N_PAIR * charge_anion)

mass_cation = [
    14.007, 1.008, 1.008, 1.008, 12.011, 1.008, 1.008, 12.011, 1.008, 1.008,
    1.008
]
mass_anion = [14.007, 15.999, 15.999, 15.999]
masses = jnp.array(N_PAIR * mass_cation + N_PAIR * mass_anion)

type_cation = ["N", "H", "H", "H", "C", "H", "H", "C", "H", "H", "H"]
type_anion = ["N", "O", "O", "O"]
types = N_PAIR * type_cation + N_PAIR * type_anion
unique_types = sorted(set(types))
type_dict = collections.OrderedDict()
for i, k in enumerate(unique_types):
    type_dict[k] = i
types = jnp.array([type_dict[i] for i in types])
n_atoms = len(types)

print("- Unpickling the Coulomb forces")
coulomb_forces = pickle.load(
    open(
        (pathlib.Path(__file__).parent /
         "../data_manuscript/coulomb_forces.pickle").resolve(), "rb"
    )
)
print("- Unpickling the Coulomb energies")
coulomb_energies = pickle.load(
    open(
        (
            pathlib.Path(__file__).parent /
            "../data_manuscript/coulomb_energies.pickle"
        ).resolve(), "rb"
    )
)

print("- Reading the JSON file")
cells = []
positions = []
energies = []
forces = []
energies_atomiccharges = []
forces_atomiccharges = []
with open(
    (pathlib.Path(__file__).parent /
     "../data_manuscript/configurations.json").resolve(), "r"
) as json_f:
    for line, coulomb_e, coulomb_f in zip(
        json_f, coulomb_energies, coulomb_forces
    ):
        json_data = json.loads(line)
        cells.append(jnp.diag(jnp.array(json_data["Cell-Size"])))
        positions.append(json_data["Positions"])
        energies.append(json_data["Energy"])
        forces.append(json_data["Forces"])
        energies_atomiccharges.append(json_data["Energy"] - coulomb_e)
        forces_atomiccharges.append(json_data["Forces"] - coulomb_f)
print("- Done")

n_configurations = len(positions)
types = [types for i in range(n_configurations)]

order = list(range(n_configurations))
random.shuffle(order)
cells = jnp.array([cells[i] for i in order])
positions = jnp.array([positions[i] for i in order])
energies = jnp.array([energies[i] for i in order])
types = jnp.array([types[i] for i in order])
forces = jnp.array([forces[i] for i in order])
energies_atomiccharges = jnp.array([energies_atomiccharges[i] for i in order])
forces_atomiccharges = jnp.array([forces_atomiccharges[i] for i in order])

print(f"- {n_configurations} configurations are available")

n_training = int(TRAINING_FRACTION * n_configurations)
n_validation = n_configurations - n_training
print(f"\t- {n_training} will be used for training")
print(f"\t- {n_validation} will be used for validation")
n_types = int(types.max()) + 1

# %%
max_neighbors = 1
for p, c in zip(positions, cells):
    max_neighbors = max(
        max_neighbors,
        get_max_number_of_neighbors(jnp.asarray(p), R_CUT, jnp.asarray(c))
    )
print("- Maximum number of neighbors that must be considered: ", max_neighbors)
pipeline = PowerSpectrumGenerator(N_MAX, R_CUT, n_types, max_neighbors)

descriptors = pipeline(positions[0], types[0], cells[0])
n_descriptors = descriptors.shape[1] * descriptors.shape[2]

cells_train = cells[:n_training]
positions_train = positions[:n_training]
types_train = types[:n_training]
energies_train = energies[:n_training]
forces_train = forces[:n_training]
energies_atomiccharges_train = energies_atomiccharges[:n_training]
forces_atomiccharges_train = forces_atomiccharges[:n_training]

cells_validate = cells[n_training:]
positions_validate = positions[n_training:]
types_validate = types[n_training:]
energies_validate = energies[n_training:]
forces_validate = forces[n_training:]
energies_atomiccharges_validate = energies_atomiccharges[n_training:]
forces_atomiccharges_validate = forces_atomiccharges[n_training:]

# %%
average_dft_energy_train = energies_train.mean()
average_dft_energy_atomiccharges_train = energies_atomiccharges_train.mean()

# %%
core_model = Core([64, 32, 16, 16, 16])
dynamics_model = DynamicsModel(
    n_types, EMBED_D, pipeline.process_data, core_model
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_NEURALIL.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("NeuralIL:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)

# %%
core_model = Core([64, 32, 16, 16, 16])
dynamics_model = DynamicsModel(
    n_types, EMBED_D, pipeline.process_data, core_model
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_ATOMICCHARGES.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_atomiccharges_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train,
    energies_atomiccharges_train, forces_atomiccharges_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_atomiccharges_validate, forces_atomiccharges_validate
)

print("AtomicCharges:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)

# %%
pickle_file = "params_ENERGY.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)

average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("EnergyOnly:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)

# %%
core_model = Core([64, 32, 16, 16, 16])
dynamics_model = NoEmbeddingDynamicsModel(
    n_types, pipeline.process_data, core_model
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_NOEMBEDDING.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("NoEmbedding:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)

# %%
core_model = Core([64, 32, 16, 16, 16])
dynamics_model = ChiEmbeddingDynamicsModel(
    n_types, pipeline.process_data, core_model
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_CHIEMBEDDING.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("ChiEmbedding:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)
# %%
core_model = Core([64, 32, 16, 16, 16])
dynamics_model = ZEmbeddingDynamicsModel(
    n_types, pipeline.process_data, core_model
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_ZEMBEDDING.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("ZEmbedding:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)
# %%
core_model = Core([64, 32, 16, 16, 16])
dynamics_model = WeightEmbeddingDynamicsModel(
    n_types, pipeline.process_data, core_model
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_WEIGHTEMBEDDING.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModel.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("WeightEmbedding:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)
# %%
core_model = Core([64, 32, 16, 16, 16])

covalent_radii = {'C': 0.76, 'N': 0.71, 'O': 0.66, 'H': 0.31}
sigmas = jnp.asarray([covalent_radii[key] for key in type_dict])

core_model_electrostatic = Core([16, 16, 16])
electrostatic_model = ElectrostaticModel(
    core_model_electrostatic, n_types, sigmas
)
dynamics_model = DynamicsModelWithCharges(
    n_types, EMBED_D, pipeline.process_data, core_model, electrostatic_model
)
rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModelWithCharges.calc_forces
)

optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)

pickle_file = "params_CHARGES.pickle"
with open(pickle_file, "rb") as f:
    state_dict = pickle.load(f)
    optimizer = flax.serialization.from_state_dict(optimizer, state_dict)


@jax.jit
def calculate_energy(params, positions, types, cell):
    return dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModelWithCharges.calc_potential_energy
    )


average_model_energy_train = jnp.mean(
    jnp.array(
        [
            calculate_energy(optimizer.target, p, t, c)
            for (p, t, c) in zip(positions_train, types_train, cells_train)
        ]
    )
)


@jax.jit
def error_contributions(params, positions, types, cell, energy, forces):
    pred_energy = dynamics_model.apply(
        params,
        positions,
        types,
        cell,
        method=DynamicsModelWithCharges.calc_potential_energy
    )
    pred_forces = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModelWithCharges.calc_forces
    )
    delta_energy = (
        (pred_energy - average_model_energy_train) -
        (energy - average_dft_energy_train)
    )
    delta_forces = forces - pred_forces
    mse_contribution_energy = delta_energy * delta_energy
    mae_contribution_energy = jnp.fabs(delta_energy)
    mse_contribution_forces = (delta_forces * delta_forces).mean()
    mae_contribution_forces = jnp.fabs(delta_forces).mean()
    return (
        mse_contribution_energy,
        mae_contribution_energy,
        mse_contribution_forces,
        mae_contribution_forces,
    )


def eval_step(
    params, positions_batch, types_batch, cells_batch, energies_batch,
    forces_batch
):
    total_squared_error_energy = 0.
    total_absolute_error_energy = 0.
    total_squared_error_forces = 0.
    total_absolute_error_forces = 0.
    for p, t, c, e, f in zip(
        positions_batch, types_batch, cells_batch, energies_batch, forces_batch
    ):
        se_e, ae_e, se_f, ae_f = error_contributions(params, p, t, c, e, f)
        total_squared_error_energy += se_e
        total_absolute_error_energy += ae_e
        total_squared_error_forces += se_f
        total_absolute_error_forces += ae_f
    return (
        jnp.sqrt(total_squared_error_energy.sum() / len(positions_batch)) /
        positions_batch.shape[1], total_absolute_error_energy.sum() /
        len(positions_batch) / positions_batch.shape[1],
        jnp.sqrt(total_squared_error_forces.sum() / len(positions_batch)),
        total_absolute_error_forces.sum() / len(positions_batch)
    )


rmse_e_train, mae_e_train, rmse_f_train, mae_f_train = eval_step(
    optimizer.target, positions_train, types_train, cells_train, energies_train,
    forces_train
)

rmse_e_validate, mae_e_validate, rmse_f_validate, mae_f_validate = eval_step(
    optimizer.target, positions_validate, types_validate, cells_validate,
    energies_validate, forces_validate
)

print("Including charge equilibration:")
print(
    f"TRAIN:\n"
    f"\tRMSE = {rmse_e_train} eV/atom, {rmse_f_train} eV/Å\n"
    f"\tMAE = {mae_e_train} eV/atom, {mae_f_train} eV/Å"
)
print(
    f"VALIDATION:\n"
    f"\tRMSE = {rmse_e_validate} eV/atom, {rmse_f_validate} eV/Å\n"
    f"\tMAE = {mae_e_validate} eV/atom, {mae_f_validate} eV/Å"
)