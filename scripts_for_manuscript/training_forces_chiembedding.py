#!/usr/bin/env python

import pathlib
import json
import pickle
import random
import collections

import tqdm
import tqdm.auto
import jax
import jax.nn
import jax.numpy as jnp
import flax
import flax.optim
import flax.jax_utils
import flax.serialization

from neuralil.neural_network.bessel_descriptors import (
    get_max_number_of_neighbors
)
from neuralil.neural_network.bessel_descriptors import PowerSpectrumGenerator
from neuralil.neural_network.model import Core
from neuralil.neural_network.model import (
    ChiEmbeddingDynamicsModel as DynamicsModel
)

random.seed(31337)

N_PAIR = 15

TRAINING_FRACTION = .9
R_CUT = 3.5
N_MAX = 4

N_EPOCHS = 501
N_BATCH = 8

LOG_COSH_PARAMETER = 1e1  # In angstrom / eV


def create_log_cosh(parameter):
    """Return a differentiable implementation of the log cosh loss.

    Args:
        parameter: The positive parameter determining the transition between
            the linear and quadratic regimes.

    Returns:
        A float->float function to compute a contribution to the loss.

    Raises:
        ValueError: If parameter is not positive.
    """
    if parameter <= 0.:
        raise ValueError("parameter must be positive")

    def nruter(x):
        return (
            (jax.nn.softplus(2. * parameter * x) - jnp.log(2)) / parameter - x
        )

    return nruter


log_cosh = create_log_cosh(LOG_COSH_PARAMETER)

mass_cation = [
    14.007, 1.008, 1.008, 1.008, 12.011, 1.008, 1.008, 12.011, 1.008, 1.008,
    1.008
]
mass_anion = [14.007, 15.999, 15.999, 15.999]
masses = jnp.array(N_PAIR * mass_cation + N_PAIR * mass_anion)

type_cation = ["N", "H", "H", "H", "C", "H", "H", "C", "H", "H", "H"]
type_anion = ["N", "O", "O", "O"]
types = N_PAIR * type_cation + N_PAIR * type_anion
unique_types = sorted(set(types))
type_dict = collections.OrderedDict()
for i, k in enumerate(unique_types):
    type_dict[k] = i
types = jnp.array([type_dict[i] for i in types])
n_atoms = len(types)

print("- Reading the JSON file")
cells = []
positions = []
energies = []
forces = []
with open(
    (pathlib.Path(__file__).parent /
     "../data_manuscript/configurations.json").resolve(), "r"
) as json_f:
    for line in json_f:
        json_data = json.loads(line)
        cells.append(jnp.diag(jnp.array(json_data["Cell-Size"])))
        positions.append(json_data["Positions"])
        energies.append(json_data["Energy"])
        forces.append(json_data["Forces"])
print("- Done")

n_configurations = len(positions)
types = [types for i in range(n_configurations)]

order = list(range(n_configurations))
random.shuffle(order)
cells = jnp.array([cells[i] for i in order])
positions = jnp.array([positions[i] for i in order])
energies = jnp.array([energies[i] for i in order])
types = jnp.array([types[i] for i in order])
forces = jnp.array([forces[i] for i in order])

print(f"- {n_configurations} configurations are available")

n_training = int(TRAINING_FRACTION * n_configurations)
n_validation = n_configurations - n_training
print(f"\t- {n_training} will be used for training")
print(f"\t- {n_validation} will be used for validation")
n_types = int(types.max()) + 1

max_neighbors = 1
for p, c in zip(positions, cells):
    max_neighbors = max(
        max_neighbors,
        get_max_number_of_neighbors(jnp.asarray(p), R_CUT, jnp.asarray(c))
    )
print("- Maximum number of neighbors that must be considered: ", max_neighbors)
pipeline = PowerSpectrumGenerator(N_MAX, R_CUT, n_types, max_neighbors)

descriptors = pipeline(positions[0], types[0], cells[0])
n_descriptors = descriptors.shape[1] * descriptors.shape[2]

cells_train = cells[:n_training]
positions_train = positions[:n_training]
types_train = types[:n_training]
energies_train = energies[:n_training]
forces_train = forces[:n_training]

cells_validate = cells[n_training:]
positions_validate = positions[n_training:]
types_validate = types[n_training:]
energies_validate = energies[n_training:]
forces_validate = forces[n_training:]


def create_onecycle_schedule(lr_min, lr_max, lr_final, steps_per_epoch):
    cycle_length = int(.9 * steps_per_epoch)

    def learning_rate_fn(step):
        slope = 2. * (lr_max - lr_min) / cycle_length
        if step < cycle_length // 2:
            return lr_min + slope * step
        elif step < cycle_length:
            return lr_max - slope * (step - cycle_length // 2)
        else:
            return lr_final

    return learning_rate_fn


instance_code = "CHIEMBEDDING"

core_model = Core([64, 32, 16, 16, 16])
dynamics_model = DynamicsModel(
    n_types,
    pipeline.process_data,
    core_model,
)

rng = jax.random.PRNGKey(0)
rng, init_rng = jax.random.split(rng)
params = dynamics_model.init(
    init_rng,
    positions_train[0],
    types_train[0],
    cells_train[0],
    method=DynamicsModel.calc_forces
)

# This learning rate will never be used in practice because of the scheduler.
optimizer_def = flax.optim.Adam(learning_rate=1.)
optimizer = optimizer_def.create(params)


@jax.jit
def train_step(
    optimizer, positions_batch, types_batch, cells_batch, forces_batch,
    learning_rate
):
    def calc_loss(params):
        def contribution(positions, types, cell, forces):
            pred = dynamics_model.apply(
                params,
                positions,
                types,
                cell,
                method=DynamicsModel.calc_forces
            )
            delta = forces - pred
            return log_cosh(delta).mean()

        return jnp.mean(
            jax.vmap(jax.checkpoint(contribution)
                    )(positions_batch, types_batch, cells_batch, forces_batch),
            axis=0
        )

    calc_loss_and_grad = jax.value_and_grad(calc_loss)
    loss_val, loss_grad = calc_loss_and_grad(optimizer.target)
    optimizer = optimizer.apply_gradient(loss_grad, learning_rate=learning_rate)
    return loss_val, optimizer


@jax.jit
def error_contributions(params, positions, types, cell, forces):
    pred = dynamics_model.apply(
        params, positions, types, cell, method=DynamicsModel.calc_forces
    )
    mse_contribution = ((forces - pred) * (forces - pred)).mean()
    mae_contribution = jnp.fabs(forces - pred).mean()
    return (mse_contribution, mae_contribution)


def eval_step(params, positions_batch, types_batch, cells_batch, forces_batch):
    total_squared_error = 0.
    total_absolute_error = 0.
    for p, t, c, f in zip(
        positions_batch, types_batch, cells_batch, forces_batch
    ):
        squared_error, absolute_error = error_contributions(params, p, t, c, f)
        total_squared_error += squared_error
        total_absolute_error += absolute_error
    return (
        jnp.sqrt(total_squared_error.sum() / len(positions_batch)),
        total_absolute_error.sum() / len(positions_batch)
    )


def train_epoch(optimizer, batch_size, epoch, rng):
    steps_per_epoch = n_training // batch_size

    perms = jax.random.permutation(rng, n_training)
    perms = perms[:steps_per_epoch * batch_size]
    perms = perms.reshape((steps_per_epoch, batch_size))
    learning_schedule = create_onecycle_schedule(
        1e-3, 1e-2, 1e-5, steps_per_epoch
    )

    with tqdm.auto.trange(len(perms)) as t:
        t.set_description(f"EPOCH #{epoch + 1}")
        for iperm in t:
            perm = perms[iperm].sort()
            positions_batch = positions_train[perm, ...]
            types_batch = types_train[perm, ...]
            cells_batch = cells_train[perm, ...]
            forces_batch = forces_train[perm, ...]
            learning_rate = learning_schedule(iperm)
            loss, optimizer = train_step(
                optimizer, positions_batch, types_batch, cells_batch,
                forces_batch, learning_rate
            )
            t.set_postfix(loss=loss, learning_rate=learning_rate)

    return optimizer


PICKLE_FILE = f"params_{instance_code}.pickle"

min_mae = jnp.inf
for i in range(N_EPOCHS):
    rng, epoch_rng = jax.random.split(rng)
    optimizer = train_epoch(optimizer, N_BATCH, i, epoch_rng)
    rmse, mae = eval_step(
        optimizer.target, positions_validate, types_validate, cells_validate,
        forces_validate
    )
    print(f"VALIDATION: RMSE = {rmse} eV/A. MAE = {mae} eV/A.")

    # Save the state only if the validation MAE is minimal.
    dict_output = flax.serialization.to_state_dict(optimizer)
    if mae < min_mae:
        with open(PICKLE_FILE, "wb") as f:
            print("- Saving the most recent state")
            pickle.dump(dict_output, f, protocol=5)
        min_mae = mae
