# NeuralIL: A Differentiable Neural-Network Force Field for Ionic Liquids

This repository contains the supporting code and data for the manuscript *A Differentiable Neural-Network Force Field for Ionic Liquids*. Its most important parts are the following:

## A JAX-backed implementation of the spherical Bessel descriptors

This family of descriptors was introduced by Kocer *et al.* in

[Kocer, E.; Mason, J. K.; Erturk, H. Continuous and Optimally Complete Description of
Chemical Environments using Spherical Bessel Descriptors. AIP Adv. 2020, 10, 015021.](https://doi.org/10.1063/1.5111045)

A C-backed [reference implementation](https://github.com/harharkh/sb_desc) has been released by the authors. Our implementation, based on [JAX](https://github.com/google/jax) and fully differentiable, can be found in the `src/neuralil/neural_network/bessel_descriptors.py` file and the `src/neuralil/neural_network/spherical_bessel` subdirectory. It includes an implementation of the spherical Bessel functions themselves.

## The NeuralIL model

Our model for the potential energy of the system, as explained in the manuscript, is implemented as a [Flax](https://github.com/google/flax) module. The relevant classes can be found in the file `src/neuralil/neural_network/model.py`.

To install the Python module, run

```bash
python setup.py install
```

or

```bash
python setup.py develop
```

from the main directory.

## Ab-initio data for training and validation

In the manuscript, the DFT results for training and validation are provided in two separate json files. The same data is included in this repository in a single file, `data_manuscript/configurations.json`, with the splitting done in the training scripts.

Moreover, `data_manuscript/configurations_opls.json` contains results for the same configurations computed using the classical OPLS-AA force field. Finally, `data_manuscript/coulomb_energies.pickle` and `data_manuscript/coulomb_forces.pickle` store the Coulomb forces and energies in those configurations calculated according to an atomic point charge model, using [this implementation](https://github.com/fweik/p3m-standalone) of the P3M method.

## Training scripts and stored models
Scripts that can be used to train the models described in the manuscript can be found in the `scripts_for_manuscript` subdirectory, with names following the pattern `training_*.py`. Corresponding sets of parameters are also provided in the same directory, with filenames `params_*.pickle`.

## Evaluation scripts
The script `scripts_for_manuscript/compute_statistics.py` loads the stored parameters and computes basic statistics for the models, with the exception of the "deep sets" architecture, which is handled by the script `scripts_for_manuscript/twostate_statistics.py`.
